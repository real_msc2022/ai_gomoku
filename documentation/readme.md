#### AIA gomoku kickoff
##### Compilation
pyinstaller.exe main.py protocol.py --name pbrain-gomoku-ai_test.exe --onefile

###### AI main methods
aleatoire
min max (bcp de possibilites)
monte carlo
machine learning

###### Code
Python > Py installer créé les points exe
Ok pour numpy
C/C++ > Cmake et Makefile

###### Rules
Contrainte 5sec poour jouer.
PAs d'accès à PIP (lib python) 
protocole piskvork

###### How to debug PV
comment débugger : ecrire dans un ichier.
ou console piskvork

###### TA advise
Bot tester les sorties.
Pas de sklearn ni tensorflow.
Python v spécifique et Epitest docker.

###### Bootstrap

- gerer nimporte quel type de taille avec start (doit construire la map)
- begin 
- info osef
- board (x,y,1) ou (x,y,0)
- end fin tu tour
python : transpilation via py-installer

- gere IO via un thread (du coup y'en a plusieurs)
- class manager global standard
- class IA (deportée sur un thread - surtout un min max)
- pas de ML car pas de sklearn et dataset à faire.
- modele ML headon sans tensorflow.
- moins de 70 Mo >> hardcoder toutes les positions c'est chaud.
- conseil minMax >> optimisation Alpha Beta > determiner la meilleure branche.
- conseil montecarlo - a eviter car aleatoire
- fitness function = donne la valeur d'un candidat
- softmax = proba de la valeur de chaque candidat
- pattern matching = suivre une série de règle (sur un bloc de 5x5)
- récupérer pattern depuis un bot qui joue à PV >> trouver un bot qui marche

###### Docker
- docker run
- mount /mnt/data
docker contient tous les programmes

###### install procedure
Uninstall older Python != 3.7.x
DL PIP
Powershell: Pip install pyinstaller
DL & Install python 3.7.5
Install VSCode & Python ext

get_line()

start
info

turn
info

sys.stdout.write