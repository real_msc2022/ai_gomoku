#!/usr/bin/python3
from sys import stdout, exit
from random import randint # pylint: disable=unused-import
import numpy # pylint: disable=unused-import

#===
# REM :   board ((y,x))
# Issues: boarder issue for test (0 or size)
#===


class Protocol_funct:
    size = 10
    board = numpy.zeros((size,size))
    timeout_turn = 0
    timeout_match = 0
    max_memory = 0
    time_left = 0
    game_type = 0
    rule = 0
    evaluate = 0
    folder = 0
    a = randint(0, size)
    b = randint(0, size)
    
# MANDATORY INIT
    def __init__(self):
        self.start()

# MANDATORY START
    def start(self):
        while True:
            commandMessage = input().split(" ")
            command = commandMessage[0]
            self.function_dictionary[command](self, commandMessage)


    # SETTER FUNCTION FOR INFO
    def setTurnTimeout(self, arg):
        self.timeout_turn = arg[1]

    def setMatchTimeout(self, arg):
        self.timeout_match = arg[1]

    def setMaxMemory(self, arg):
        self.max_memory = arg[1]

    def setTimeLeft(self, arg):
        self.time_left = arg[1]

    def setGameType(self, arg):
        if len(arg) == 1:
            return
        self.game_type = arg[1]

    def setRule(self, arg):
        if len(arg) == 1:
            return
        self.rule = arg[1]

    def setEvaluate(self, arg):
        if len(arg) == 1:
            return
        self.evaluate = arg[1]

    def setFolder(self, arg):
        if len(arg) == 1:
            return
        self.folder = arg[1]


    #REAL FUNCTION
    def infoFunction(self, arg):
        if not arg:
            return
        data = arg[1]
        # send response
        self.info_dictionary[data](self, arg) if data in self.info_dictionary else print("ERROR message - Unknowed info = ignored")
    
    # START size - test with START 19
    def startFunction(self, arg):
        if (int(arg[1]) > 5 and int(arg[1]) < 100):
            self.size = int(arg[1])
            self.board = numpy.zeros((self.size,self.size))
            stdout.write("OK\n")
            stdout.flush()
        else :
            stdout.write("ERROR message - unsupported size or other error\n")
            stdout.flush()
        return 0
        
    def beginFunction(self, arg):
        Protocol_funct.play(self)
        return 0

    def turnFunction(self, arg):
        coor = arg[1].split(",")
        x = int(coor[0])
        y = int(coor[1])
        self.board[x][y] = 2
        Protocol_funct.play(self)
        return 0

    def boardFunction(self, arg):
        while True:
            boardinput = input()
            if boardinput == "DONE":
                break
            coor = boardinput.split(",")
            x = int(coor[0])
            y = int(coor[1])
            self.board[x][y] = int(coor[2])
        Protocol_funct.play(self)
        return 0

########################
# Limit of protocol    #
########################

    #IA FUNCTION - RANDOMIZED PART
    def play(self):
        x, y = Protocol_funct.checkBoard(self)
        self.board[x][y] = 1
        #Protocol_funct.displayBoard(self)
        stdout.write(str(x) + "," + str(y) + "\n")
        stdout.flush()
        return 0

    def chooseFirstCoor(self):
        self.a += 1
        self.b += 1
        if Protocol_funct.isFree(self, self.a, self.b):
            return self.a,self.b
        else:
            self.a = randint(0, self.size -5)
            self.b = randint(0, self.size -5)
            return Protocol_funct.chooseRandomCoor(self)

    def chooseRandomCoor(self):
        x = randint(0, self.size -1)
        y = randint(0, self.size -1)
        if Protocol_funct.isFree(self, x, y):
            return x,y
        else:
            Protocol_funct.chooseRandomCoor(self)

    def isFree(self, x, y):
        if x < self.size and y < self.size:
            if self.board[x][y] == 0:
                return True
            else:
                return False
        else:
            return False

    def displayBoard(self):
        for i in range (0, self.size -1):
            for j in range (0, self.size -1):
                stdout.write(str(self.board[i][j]) + " ")
            stdout.write(".\n")

    #IA DEFENCE FUNCTION
    # Function needs rework
    def checkBoard(self):
        for i in range (0, self.size -4):
            for j in range (0, self.size -4):
                if self.board[i][j] == 0:
                    self.board[i][j] = 2
                    #if Protocol_funct.checkFive(self, i, j):
                    #   return i,j
                    if Protocol_funct.checkFour(self, i, j):
                        self.board[i][j] = 0
                        return i,j
                    self.board[i][j] = 0                    
                    #if Protocol_funct.checkThree(self, i, j)
                    #    return i,j
        
        return Protocol_funct.chooseFirstCoor(self)

# Test functions : pattern and direction

    def checkFive(self, x, y):
        #check vertical
        if self.board[x+1][y] == self.board[x+2][y] == self.board[x+3][y] == self.board[x+4][y] == 2:
            return True
        #check horizontal
        if self.board[x][y+1] == self.board[x][y+2] == self.board[x][y+3] == self.board[x][y+4] == 2:
            return True
        #check diag dir 1
        if self.board[x+1][y+1] == self.board[x+2][y+2] == self.board[x+3][y+3] == self.board[x+4][y+4] == 2:
            return True        
        #check diag dir 2
        if self.board[x-1][y-1] == self.board[x-2][y-2] == self.board[x-3][y-3] == self.board[x-4][y-4] == 2:
            return True  
        #check diag dir 3
        if self.board[x+1][y-1] == self.board[x+2][y-2] == self.board[x+3][y-3] == self.board[x+4][y-4] == 2:
            return True        
        #check diag dir 4
        if self.board[x-1][y+1] == self.board[x-2][y+2] == self.board[x-3][y+3] == self.board[x-4][y+4] == 2:
            return True
        return False      

    def checkFour(self, x, y):
        saveX = x
        saveY = y

        #check vertical
        while self.board[x-1][y] == 2:
            x -= 1
        if self.board[x+1][y] == self.board[x+2][y] == self.board[x+3][y] == 2:
            return True

        #check horizontal
        x = saveX
        y = saveY
        while self.board[x][y-1] == 2:
            y -= 1
        if self.board[x][y+1] == self.board[x][y+2] == self.board[x][y+3] == 2:
            return True

        #check diag 1
        x = saveX
        y = saveY
        while self.board[x-1][y-1] == 2:
            y -= 1
            x -= 1
        if self.board[x+1][y+1] == self.board[x+2][y+2] == self.board[x+3][y+3] == 2:
            return True     

        #check diag 2
        x = saveX
        y = saveY
        while self.board[x-1][y+1] == 2:
            y += 1
            x -= 1
        if self.board[x+1][y-1] == self.board[x+2][y-2] == self.board[x+3][y-3] == 2:
            return True        
        return False          

    #MANAGER DICTIONNARY
    function_dictionary = {
        "START":startFunction,
        "TURN":turnFunction,
        "BEGIN":beginFunction,
        "BOARD":boardFunction,
        "INFO":infoFunction
    }

    info_dictionary = {
        "timeout_turn":setTurnTimeout,
        "timeout_match":setMatchTimeout,
        "max_memory":setMaxMemory,
        "time_left":setTimeLeft,
        "game_type":setGameType,
        "rule":setRule,
        "evaluate":setEvaluate,
        "folder":setFolder
    }
